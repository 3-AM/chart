// get vars form GET
function getUrlVars() {
    var vars = [];
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars.push(value);
    });
    return vars;
}

// prepare data for file
function prepareOutput(data, labels){
    if (data.length != labels.length) {
        console.log("Error! Length is not the same");
        return ""
    } else {
        var text = "";
        for (i = 0; i < data.length; i++) {
            text += labels[i] + ": " + data[i] + " ";
        }
        return text;
    }
}

function findMin(data) {
    var minIndex = 0;
    var min = data[0];

    for (var i = 1; i < data.length; i++) {
        if (data[i] < min) {
            minIndex = i;
            min = data[i];
        }
    // huminize
    }
    return minIndex + 1;
}

// check if object is empty
function isEmpty(o) {
    for (var p in o) {
        if ( o.hasOwnProperty( p ) ) { return false; }
    }
    return true;
}


// initial graph data
var initial_data = {
    labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running", 'Playing'],
    datasets: [
        {
            label: "My Second dataset",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            //data: [10, 4, 8, 6, 3, 9, 7, 0]
            // data: []
        }
    ]
};

// =====================================================================
if (!isEmpty(getUrlVars())) {
    // build graph if form is submited
    var ctx = document.getElementById("formChart").getContext("2d");
    initial_data['datasets'][0]['data'] = getUrlVars();
    var myRadarChart = new Chart(ctx).Radar(initial_data);

    $("#statsform").hide();
    $("#mailform").show();

    // convert data to string and add to form, so we can write it to file with other unser data
    var graph_data = prepareOutput(initial_data['datasets'][0]['data'], initial_data['labels']);
    var graphdata_input = document.getElementById("graphdata");
    graphdata_input.value = graph_data;

    // redirect
    var min_pos = findMin(getUrlVars());
    document.getElementById('redirect_to').value = "statpages/" + min_pos + ".html";

} else {
    console.log("not submited");
}
