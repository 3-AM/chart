<?php

// save data from mailform to data.txt
if(isset($_POST['fullname']) && isset($_POST['email'])) {
    $data = $_POST['fullname'] . ' | ' . $_POST['email'] . " | " . $_POST['graphdata'] . "\n";
    $ret = file_put_contents('data.txt', $data, FILE_APPEND | LOCK_EX);
    if($ret === false) {
        die('There was an error writing this file');
    }
    else {
        header("Location: " . $_POST['redirect_to']);
        die();
    }
}
else {
   die('no post data to process');
}
